import React, { Component } from 'react';
import './PieChart.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import moment, { unix } from 'moment';


class PieChart extends Component {
  constructor(props) {
    super(props);
  }

  generateOptionsToCreatChartData() {
    return {
      // legend: {
      //   enabled: false
      // },
      legend: {
        align: 'right',
        verticalAlign: 'top',
        layout: 'vertical',
        x: 0,
        y: 100
      },
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
      },
      title: {
        text: "<div style='text-align: center; font-size: 20px; margin-top: -30px;'> <div>9600</div> <div>Transactions</div></div>",
        align: 'center',
        verticalAlign: 'middle',
        y: 30,
        // x: -40,
        useHTML: true,
        // formatter: function() {
        //   return "<div style='text-align: center; font-size: 20px; margin-top: -30px;'> <div>9600</div> <div>Transactions</div></div>";
        // },
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: 'bold',
              color: 'white'
            }
          },
          startAngle: 0,
          endAngle: 360,
          center: ['50%', '50%'],
          // showInLegend: true
        }
      },
      series: [{
        type: 'pie',
        // name: 'Browser share',
        innerSize: '50%',
        data: [
          ['Store 1', 60],
          ['Store 2', 25],
          ['Store 3', 15],

        ]
      }]
    }
  }
  render() {
    const options = this.generateOptionsToCreatChartData();
    return (
      <HighchartsReact
        highcharts={Highcharts}
        options={options}
      />
    )
  }
}

export default PieChart;




