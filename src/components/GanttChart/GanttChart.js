import React, { Component } from 'react';
import './GanttChart.css';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import xRange from "highcharts/modules/xrange";
import highchartsMore from "highcharts/highcharts-more";

highchartsMore(Highcharts);
xRange(Highcharts);





const firstChartData = [{
  x: Date.UTC(2012, 5, 22, 8, 15),
  x2: Date.UTC(2012, 5, 22, 8, 20),
  y: 0,
  // partialFill: 0.25
}, {
  x: Date.UTC(2012, 5, 22, 8, 25),
  x2: Date.UTC(2012, 5, 22, 8, 30),
  y: 1
}, {
  x: Date.UTC(2012, 5, 22, 8, 35),
  x2: Date.UTC(2012, 5, 22, 8, 40),
  y: 2
}, {
  x: Date.UTC(2012, 5, 22, 8, 45),
  x2: Date.UTC(2012, 5, 22, 8, 50),
  y: 1
}, {
  x: Date.UTC(2012, 5, 22, 8, 55),
  x2: Date.UTC(2012, 5, 22, 8, 60),
  y: 2
}];

const secondChartData = [{
  x: Date.UTC(2012, 5, 22, 9, 5),
  x2: Date.UTC(2012, 5, 22, 9, 10),
  y: 2,
  // partialFill: 0.25
}, {
  x: Date.UTC(2012, 5, 22, 9, 15),
  x2: Date.UTC(2012, 5, 22, 9, 20),
  y: 0
}, {
  x: Date.UTC(2012, 5, 22, 9, 25),
  x2: Date.UTC(2012, 5, 22, 9, 30),
  y: 1
}, {
  x: Date.UTC(2012, 5, 22, 9, 35),
  x2: Date.UTC(2012, 5, 22, 9, 40),
  y: 2
}, {
  x: Date.UTC(2012, 5, 22, 9, 45),
  x2: Date.UTC(2012, 5, 22, 9, 50),
  y: 1
}];


class GanttChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: {
        chart: {
          type: 'xrange'
        },
        title: {
          text: 'Highcharts X-range'
        },
        xAxis: {
          type: 'datetime',
          dateTimeLabelFormats: {
            hour: '%I %p',
            minute: '%I:%M %p',
  
          },
          opposite: true
        },
  
        yAxis: {
          title: {
            text: ''
          },
          categories: ['Prototyping', 'Development', 'Testing'],
          reversed: true
        },
        series: [{
          name: 'Project 1',
          // pointPadding: 0,
          // groupPadding: 0,
          borderColor: 'gray',
          pointWidth: 20,
          data: [...firstChartData],
          dataLabels: {
            enabled: true
          }
        }],
        credits: {
          enabled: false
        },
      }
    }
    this.onClick = this.onClick.bind(this);
  }

  onClick(data) {
    const component = this;
    console.log(data);
    setTimeout(function(){
      component.setState({
        options: {
          series: [{
            data: [...data]
          }]
        }
      });
    }, 100);
  }

  componentWillUnmount() {
    clearTimeout(this.interval)
  }

  generateOptionsToCreatChartData() {
    return {
      chart: {
        type: 'xrange'
      },
      title: {
        text: 'Highcharts X-range'
      },
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
          hour: '%I %p',
          minute: '%I:%M %p',

        },
        opposite: true
      },

      yAxis: {
        title: {
          text: ''
        },
        categories: ['Prototyping', 'Development', 'Testing'],
        reversed: true
      },
      series: [{
        name: 'Project 1',
        // pointPadding: 0,
        // groupPadding: 0,
        borderColor: 'gray',
        pointWidth: 20,
        data: this.state.data,
        dataLabels: {
          enabled: true
        }
      }],
      credits: {
        enabled: false
      },
    }
  }
  render() {
    // const options = this.generateOptionsToCreatChartData();
    console.log('firstChartData', firstChartData)
    return (
      <div>
        <button onClick={() => this.onClick(firstChartData)}> {`< Pre `}</button>
        <button onClick={() => this.onClick(secondChartData)}> {`Next > `} </button>
        <HighchartsReact
          highcharts={Highcharts}
          options={this.state.options}
        />
      </div>
    )
  }
}

export default GanttChart;




