import React, { Component } from 'react';
import './SolidGaugeChart.css';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import moment, { unix } from 'moment';
import solidGauge from "highcharts/modules/solid-gauge";
import highchartsMore from "highcharts/highcharts-more";

highchartsMore(Highcharts);
solidGauge(Highcharts);

class SolidGaugeChart extends Component {
  constructor(props) {
    super(props);
  }

  generateOptionsToCreatChartData() {
    return {
      legend: {
        align: 'right',
        verticalAlign: 'top',
        layout: 'vertical',
        x: 0,
        y: 100
    },
      chart: {
        type: 'solidgauge',
        backgroundColor: 'transparent',
      },

      title: {
        text: '',
        style: {
          fontSize: '24px'
        }
      },
      tooltip: {
        enabled: false
      },
      // tooltip: {
      //   borderWidth: 0,
      //   backgroundColor: 'none',
      //   shadow: false,
      //   style: {
      //     fontSize: '16px'
      //   },
      //   valueSuffix: '%',
      //   pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
      //   positioner: function (labelWidth) {
      //     return {
      //       x: (this.chart.chartWidth - labelWidth) / 2,
      //       y: (this.chart.plotHeight / 2) + 15
      //     };
      //   }
      // },

      pane: {
        center: ['50%', '50%'],
        startAngle: 0,
        endAngle: 360,
        background: [
          { // Track for Move
            outerRadius: '100%',
            innerRadius: '95%',
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0])
              .setOpacity(0.3)
              .get(),
            borderWidth: 0
          },
          { // Track for Exercise
            outerRadius: '92%',
            innerRadius: '87%',
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1])
              .setOpacity(0.3)
              .get(),
            borderWidth: 0
          },
          { // Track for Stand
            outerRadius: '84%',
            innerRadius: '79%',
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2])
              .setOpacity(0.3)
              .get(),
            borderWidth: 0
          }
        ]
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: [],
        
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true,
          useHTML: true,
          // showInLegend: true
        },
        series: {
          stacking: 'normal',
          borderWidth: 0
        }
      },

      series: [
        {
          name: 'BTC',
          data: [{
            radius: '100%',
            innerRadius: '95%',
            y: 80
          }],
          dataLabels: {
            formatter: function() {
              return "<div style='text-align: center; font-size: 20px; margin-top: -30px;'> <div>9600</div> <div>Transactions</div></div>";
            },
            enabled: true,
            useHTML: true,
            borderWidth: 0,
          },
          
        },
        {
          name: 'XRP',
          data: [{
            color: Highcharts.getOptions().colors[1],
            radius: '92',
            innerRadius: '87%',
            y: 65
          }],
          
        },
        {
          name: 'USDT',
          data: [{
            color: Highcharts.getOptions().colors[2],
            radius: '84%',
            innerRadius: '79%',
            y: 50
          }]
        }
      ],
      credits: {
        enabled: false
      },
    }
  }
  render() {
    const options = this.generateOptionsToCreatChartData();
    return (
      <HighchartsReact
        highcharts={Highcharts}
        options={options}
      />
    )
  }
}

export default SolidGaugeChart;




