import React, { Component } from 'react';
import './ColumnChart.css';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import moment, { unix } from 'moment';

const DATE_TYPE = 0;
const VALUE_TYPE = 1;
const COL_CHART_SIZE_DEFAULT = 10;

class ColumnChart extends Component {
  constructor(props) {
    super(props);
  }

  generateOptionsToCreatChartData() {
    return {
      legend: {
        // layout: 'vertical',
        // align: 'right',
        verticalAlign: 'top',
        floating: true,
        backgroundColor: '#FFFFFF',
        y: 5,
        // margin: 20

      },
      chart: {
        type: 'column'
      },
      title: {
        text: 'Daily Revenue',
        align: 'left'
      },
      xAxis: {
        categories: [
          '1',
          '2',
          '3',
          '4',
          '5',
          '6',
          '7',
          '8',
          '9',
          '10',
          '11',
          '12'
        ],
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: ''
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        },
        series: {
          borderRadius: 10
        }
      },
      series: [
        {
          name: 'Tokyo',
          data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
          color: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1
            },
            stops: [[0, "#FFA26B"], [1, "#FFE8DA"]]
          }
        },
        {
          name: 'New York',
          data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3],
          color: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1
            },
            stops: [[0, "#006AF4"], [1, "#D5E9FA"]]
          }
        }, {
          name: 'London',
          data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2],
          color: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1
            },
            stops: [[0, "#00C4A1"], [1, "#C8EFE8"]]
          }
        },
      ],
      credits: {
        enabled: false
      },
    }
  }
  render() {
    const options = this.generateOptionsToCreatChartData();
    return (
      <HighchartsReact
        highcharts={Highcharts}
        options={options}
      />
    )
  }
}

export default ColumnChart;




