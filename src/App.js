import React, { Component } from 'react';
import ColumnChart from './components/ColumnChart';
import SolidGaugeChart from './components/SolidGaugeChart';
import SemiPieChart from './components/PieChart';
import GanttChart from './components/GanttChart';
import './App.css';
class App extends Component {
  render() {
    return (
      <div>
        {/* <div className='column-chart'>
          <ColumnChart />
        </div>
        <div className='pie-chart-wrapper'>
          <div className='solid-gause-chart'>
            <SolidGaugeChart />
          </div>
          <div className='semi-pie-chart'>
            <SemiPieChart />
          </div>
        </div> */}
        <div className='gantt-chart'>
          <GanttChart />
        </div>
      </div>
    );
  }
}

export default App;
